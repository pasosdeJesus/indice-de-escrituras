/*
 * @OnlyCurrentDoc
 *
 * Índice de Escrituras App Script
 * 
 * Facilita traducir y escribir comentarios bíblicos mediante
 * el chequeo de la referencia bíblíca seleccionada, extraerla de biblegateway.com y
 * generar un índice.
 * 
 * Usa la librería cheerio (para reconocer el HTML retornado por biblegateway.com --aunque
 * es muy limitada pude entender lo suficiente para hacer rapidamente lo que se necesitaba).
 * Para poder usar este App Script con un documento agregarla
 *   https://github.com/tani/cheeriogs
 *   Script ID: 1ReeQ6WO8kKNxoaA_O0XEQ589cIrRvEBA9qcWpNqdOP17i47u6N9M5Xh0
 * 
 * Public domain. 2023. vtamara@pasosdeJesus.org
 */

/* Inicializa */
function onOpen() {
  
  var ui = DocumentApp.getUi();
  // Or DocumentApp or FormApp.
  ui.createMenu('Índice de Escrituas')
    .addItem('Presentar Barra Lateral', 'presentarBarraLateral')
    .addItem('Estadísticas del Índice', 'estadisticasIndice')
    .addItem('Indexar Referencia Bíblica', 'indexarReferenciaBíblica')
    .addToUi();
}

/* Libros bíblicos y sus abreviaturas  */
const Libro = {
  "Gé.": "Génesis",
  "Éx.": "Éxodo",
  "Lev.": "Levítico",
  "Nú.": "Números",
  "Dt.": "Deuteronomio",
  "Jos.": "Josué",
  "Jue.": "Jueces",
  "Ru.": "Ruth",
  "1 Sa.": "1 Samuel",
  "2 Sa.": "2 Samuel",
  "1 Re.": "1 Reyes",
  "2 Re.": "2 Reyes",
  "1 Cr.": "1 Crónicas",
  "2 Cr.": "2 Crónicas",
  "Esd.": "Esdras",
  "Ne.": "Nehemías",
  "Est.": "Esther",
  "Job": "Job",
  "Sal.": "Salmos",
  "Pr.": "Proverbios",
  "Ec.": "Eclesiastés",
  "C.C.": "Cantar de los Cantares",
  "Isa.": "Isaías",
  "Jer.": "Jeremías",
  "La.": "Lamentaciones",
  "Eze.": "Ezekiel",
  "Da.": "Daniel",
  "Os.": "Oseas",
  "Joel": "Joel",
  "Am.": "Amós",
  "Ab.": "Abdías",
  "Jon.": "Jonás",
  "Miq.": "Miqueas",
  "Na.": "Nahum",
  "Hab.": "Habacuc",
  "Sof.": "Sofonías",
  "Hag.": "Hageo",
  "Zac.": "Zacarías",
  "Mal.": "Malaquías",
  "Mt.": "Mateo",
  "Mc.": "Marcos",
  "Lc.": "Lucas",
  "Jn.": "Juan",
  "Hec.": "Hechos",
  "Ro.": "Romanos",
  "1 Co.": "1 Corintios",
  "2 Co.": "2 Corintios",
  "Gál.": "Gálatas",
  "Efe.": "Efesios",
  "Flp.": "Filipences",
  "Col.": "Colosenses",
  "1 Te.": "1 Tesalonicenses",
  "2 Te.": "2 Tesalonicenses",
  "1 Ti.": "1 Timoteo",
  "2 Ti.": "2 Timoteo",
  "Tit.": "Tito",
  "Flm.": "Filemón",
  "Heb.": "Hebreos",
  "Snt.": "Santiago",
  "1 Pe.": "1 Pedro",
  "2 Pe.": "2 Pedro",
  "1 Jn.": "1 Juan",
  "2 Jn.": "2 Juan",
  "3 Jn.": "3 Juan",
  "Jud.": "Judas",
  "Ap.": "Apocalipsis"
}

var urlIndice = 'https://docs.google.com/spreadsheets/d/1FpDEYD4FNcgKNw0NeOnlj0fh6KQjlm-Xm2A5LbtZq5E/edit#gid=1192987395';

function estadisticasIndice() {
  
  var ss = SpreadsheetApp.openByUrl(urlIndice);
  var nombre = ss.getName();
  
  var sheet = ss.getSheets()[0];
  if (sheet.getRange("A1").getValue() != "Page") {
    Logger.log("Se esperaba Page en A1");
  }
  if (sheet.getRange("B1").getValue() != "Row") {
    Logger.log("Se esperaba Row en B1");
  }
  if (sheet.getRange("C1").getValue() != "Column") {
    Logger.log("Se esperaba Column en C1");
  }
  if (sheet.getRange("D1").getValue() != "Reference") {
    Logger.log("Se esperaba Reference en D1");
  }
  if (sheet.getRange("E1").getValue() != "Book") {
    Logger.log("Se esperaba Book en E1");
  }
  if (sheet.getRange("F1").getValue() != "Verse(s)") {
    Logger.log("Se esperaba Verse(s) en F1");
  }
  // This logs the value in the very last cell of this sheet
  var lastRow = sheet.getLastRow();
  var dat = [];
  for( i = 2; i <= lastRow; i++) {
    let p = sheet.getRange("A" + i).getValue();
    let r = sheet.getRange("B" + i).getValue();
    let c = sheet.getRange("C" + i).getValue();
    let ref = sheet.getRange("D" + i).getValue();
    let b = sheet.getRange("E" + i).getValue();
    let v = sheet.getRange("F" + i).getValue();
    if (typeof dat[p] == "undefined") {
      dat[p] = [];
    }
    if (typeof dat[p][r] == "undefined") {
      dat[p][r] = [];
    }
    dat[p][r][c] = {ref: ref, book: b, verses: v};
  }

  DocumentApp.getUi()
          .alert(`URL del índice: ${urlIndice}\n` +
          `Nombre del archivo: ${nombre}\n` +
          `Última fila: ${lastRow}`);
}

/* Presenta Barra Lateral.
 * Tomado de https://developers.google.com/apps-script/add-ons/editors/docs/quickstart/translate#translate.gs 
 */
function presentarBarraLateral() {
  const ui = HtmlService.createHtmlOutputFromFile('barra_lateral')
      .setTitle('Índice de Escrituras');
  DocumentApp.getUi().showSidebar(ui);
}

/* Verifica la sintaxis de la referencia seleccionada, si lo es presenta el texto bíblico de RVR1960 y
 * agrega una entrada en el índice de escrituras 
 */
function indexarReferenciaBíblica() {
  let t = obtenerTextoSeleccionado();
  for (let i = 0; i < t.length; i++) {
    const r = new RegExp("^([123]?[ ]?[A-ZÁÉÍÓÚÑÜa-záéíóúñü]*[.]?) ([0-9]*[-:][0-9]*-?[0-9]*)$")
    let m = t[i].match(r);
    if (m != null) {
      if (typeof Libro[m[1]] == 'undefined') {
        DocumentApp.getUi()
          .alert(`Libro desconocido: ${m}`);
        return "";
      } else {
        let bm = t[i].replace(m[1],Libro[m[1]]);
        let c= extraerVersos(bm);
        DocumentApp.getUi()
          .alert(`Texto Bíblico: ${c}`);
        if (c != "") {

        }
        return c;
      }
    } else {
      DocumentApp.getUi()
        .alert(`Referencia con formato desconocido: ${t[i]}`);
        return "";
    }
  }
}

/* Retorna arreglo de textos seleccionados en el Google Document contenedor.
 * Tomado de https://developers.google.com/apps-script/add-ons/editors/docs/quickstart/translate#translate.gs 
 */
function obtenerTextoSeleccionado() {
  const selection = DocumentApp.getActiveDocument().getSelection();
  const text = [];
  if (selection) {
    const elements = selection.getSelectedElements();
    for (let i = 0; i < elements.length; ++i) {
      if (elements[i].isPartial()) {
        const element = elements[i].getElement().asText();
        const startIndex = elements[i].getStartOffset();
        const endIndex = elements[i].getEndOffsetInclusive();

        text.push(element.getText().substring(startIndex, endIndex + 1));
      } else {
        const element = elements[i].getElement();
        // Only translate elements that can be edited as text; skip images and
        // other non-text elements.
        if (element.editAsText) {
          const elementText = element.asText().getText();
          // This check is necessary to exclude images, which return a blank
          // text element.
          if (elementText) {
            text.push(elementText);
          }
        }
      }
    }
  }
  if (!text.length) throw new Error('Please select some text.');
  return text;
}

/* Extraer versículos de BibleGateway en version RVR1960.  
 * Comenzó a partir del ejemplo https://github.com/yassinedevop/auto-cite/blob/main/script.js 
 */
function extraerVersos(ref) {
  let ref2 = ref.replace(" ", "+").replace(":", "%3A");
  let url = "https://www.biblegateway.com/passage/?search=" + ref2 + "&version=RVR1960"
  Logger.log(url);
  let respuesta = UrlFetchApp.fetch(url);
  let contenido = respuesta.getContentText();
  
  const c = Cheerio.load(contenido);
  Logger.log(c.text());
  const cita_t = c('.passage-text p'); // Analyzing response of biblegateway.com
  let cita = cita_t.text();
  Logger.log(cita);
  return cita;
}
